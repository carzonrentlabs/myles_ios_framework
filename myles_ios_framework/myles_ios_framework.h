//
//  myles_ios_framework.h
//  myles_ios_framework
//
//  Created by Nowfal E Salam on 24/04/18.
//  Copyright © 2018 carzonrent. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for myles_ios_framework.
FOUNDATION_EXPORT double myles_ios_frameworkVersionNumber;

//! Project version string for myles_ios_framework.
FOUNDATION_EXPORT const unsigned char myles_ios_frameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <myles_ios_framework/PublicHeader.h>


