Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '8.0'
s.name = "myles_ios_framework"
s.summary = "myles_ios_framework."
s.requires_arc = true
s.version = "0.1.0"
s.license = { :type => "MIT", :file => "FILE_LICENSE" }
s.author = { "Nowfal E Salam" => "nowfal@carzonrent.com" }

s.homepage = "https://bitbucket.org/carzonrentlabs/myles_ios_framework"



s.source = { :git => "https://bitbucket.org/carzonrentlabs/myles_ios_framework.git", :tag => "#{s.version}"}


# 7
s.framework = "UIKit"

# 8
s.source_files =  "*", "myles_ios_framework/*"

# 9
#s.resources = "myles_ios_framework/**/*.{png,jpeg,jpg,storyboard,xib}"
end
